import Button from "@material-ui/core/Button";
import { useForm, Controller } from "react-hook-form";
import { TextField, Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  input: {
    height: 40,
    marginTop: 40,
  },
}));

export const AttemptForm = ({
  onFormSubmit,
  isLoading,
  refetch,
}: {
  onFormSubmit: Function;
  isLoading: boolean;
  refetch: Function;
}) => {
  const classes = useStyles();

  const { handleSubmit, control } = useForm();

  const onSubmit = handleSubmit(async (data) => {
    await onFormSubmit(data);
    refetch();
  });

  return (
    <form onSubmit={onSubmit}>
      <Box>
        <Controller
          as={
            <TextField
              label="Name"
              variant="outlined"
              size="small"
              required
              className={classes.input}
            />
          }
          name="name"
          control={control}
        />
        <Button variant="outlined" type="submit" className={classes.input}>
          {isLoading ? <p>Loading</p> : "Shoot"}
        </Button>
      </Box>
    </form>
  );
};
