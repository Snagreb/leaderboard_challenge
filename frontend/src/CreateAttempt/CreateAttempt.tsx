import { AttemptForm } from "../shared";
import { createAttempt } from "../api";
import { useMutation } from "react-query";

export const CreateAttempt = ({ refetch }: { refetch: Function }) => {
  const { mutateAsync, isLoading } = useMutation(createAttempt);

  const onFormSubmit = async (data: object) => {
    await mutateAsync({ ...data });
  };
  return (
    <AttemptForm
      onFormSubmit={onFormSubmit}
      isLoading={isLoading}
      refetch={refetch}
    />
  );
};
