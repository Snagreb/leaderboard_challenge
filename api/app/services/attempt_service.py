#!/usr/bin/env python
from models.attempt import Attempt
from config import db



def get():
    '''
    Get all entities
    :returns: all entity
    '''
    return Attempt.query.all()

def post(attempt):
    '''
    Create entity with body
    :param body: request body
    :returns: the created entity
    '''
    db.session.add(attempt)
    db.session.commit()
    return attempt




