import { useQuery } from "react-query";
import { getAllAttempts } from "../api";
import { AttemptItem } from "./AttemptItem";
import { CreateAttempt } from "../CreateAttempt/CreateAttempt";
import { makeStyles } from "@material-ui/core/styles";

import List from "@material-ui/core/List";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    display: "flex",
    flexWrap: "nowrap",
    justifyContent: "center",
  },
  input: {
    marginTop: 20,
  },
}));

export const AttemptList = () => {
  const classes = useStyles();

  const { data, isLoading, isError, refetch } = useQuery(
    "scores",
    getAllAttempts
  );

  if (isLoading) {
    return <p>Loading</p>;
  }

  if (isError) {
    return <span>Error</span>;
  }

  return (
    <div className={classes.root}>
      <div>
        <CreateAttempt refetch={refetch} />
        <List component="nav" aria-label="main mailbox folders">
          {data.map(
            ({
              id,
              name,
              score,
            }: {
              id: number;
              name: string;
              score: number;
            }) => (
              <AttemptItem name={name} score={score} key={id} id={id} />
            )
          )}
        </List>
      </div>
    </div>
  );
};
