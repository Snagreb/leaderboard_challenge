#!/usr/bin/env python
from flask_cors import CORS
from config import app
from controllers.attempt_controller import api

# register the api
app.register_blueprint(api)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
