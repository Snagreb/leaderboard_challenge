CREATE TABLE public.users
(
    id serial NOT NULL,
    name varchar(128) NOT NULL,
    score integer NOT NULL,
    CONSTRAINT users_user_pkey PRIMARY KEY (id)
);
