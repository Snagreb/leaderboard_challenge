import { AttemptList } from "./AttemptList/AttemptList";

function App() {
  return (
    <>
      <AttemptList />
    </>
  );
}

export default App;
