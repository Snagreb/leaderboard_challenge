#!/usr/bin/env python
from random import randrange
import json
from flask import Blueprint, jsonify, request
from werkzeug.exceptions import HTTPException
from flask_cors import cross_origin
import services.attempt_service as attempt_service
from models.attempt import Attempt

api = Blueprint('attempts', 'attempts')

@api.route('/attempts', methods=['GET'])
@cross_origin()
def api_get():
    ''' Get all entities'''
    attempts = attempt_service.get()
    attempts_dicts = [attempt.as_dict() for attempt in attempts]
    attempts_score_desc = sorted(attempts_dicts, key=lambda k: k['score'], reverse=True)
    return jsonify(attempts_score_desc)

@api.route('/attempts', methods=['POST'])
def api_post():
    ''' Create entity'''
    attempt = Attempt(**request.json)
    attempt.score = randrange(-1000,1001)
    attempt = attempt_service.post(attempt)
    return jsonify(attempt.as_dict())


@api.errorhandler(HTTPException)
def handle_exception(e):
    """Return JSON format for HTTP errors."""
    response = e.get_response()
    response.data = json.dumps({
        'success': False,
        "message": e.description
    })
    response.content_type = "application/json"
    return response
