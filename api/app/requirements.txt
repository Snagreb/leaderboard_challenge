PyYAML==5.1.2
Flask==1.1.1
Flask-SQLAlchemy==2.4.1
Flask-Migrate==2.5.2
psycopg2==2.8.3
flask-cors==3.0.10