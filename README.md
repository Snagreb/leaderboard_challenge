# Leaderboard Challenge

## Prerequisites

1. docker & docker-compose
2. node and npm

## How to run

1. in api folder, run `docker-compose up --build`
2. in frontend folder run `npm install && npm run start`

## Mentionable Choices

For the name input I chose react hook form: https://react-hook-form.com/. It's a little leaner than Formik and thus seemed to be the better choice in this case since we are only dealing with one textinput. It also works with controlled mui inputs.

For talking to the backend I chose React Query: https://react-query.tanstack.com/. It covers most use cases of fetching, caching and updating data without having to shoot yourself in the foot with useEffect.
