#!/usr/bin/env python
from config import db



class Attempt(db.Model):
    ''' The data model'''
    # table name
    __tablename__ = 'users'
    id = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    name = db.Column(db.String(200), nullable=False)
    score = db.Column(db.Integer(), nullable=False)
    def as_dict(self):
       return {c.name: getattr(self, c.name) for c in self.__table__.columns}
