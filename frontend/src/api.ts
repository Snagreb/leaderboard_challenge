export const getAllAttempts = async () => {
  const response = await fetch(`${process.env.REACT_APP_API_SERVER}/attempts`);

  if (!response.ok) {
    throw new Error("Something went wrong.");
  }

  return response.json();
};

export const createAttempt = async ({ ...data }) => {
  const response = await fetch(`${process.env.REACT_APP_API_SERVER}/attempts`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  });

  if (!response.ok) {
    throw new Error();
  }

  return response.json();
};
