import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";

export const AttemptItem = ({
  id,
  name,
  score,
}: {
  id: number;
  name: string;
  score: number;
}) => {
  return (
    <ListItem>
      <ListItemText primary={score} secondary={name} />
    </ListItem>
  );
};
